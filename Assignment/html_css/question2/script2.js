$( document ).ready((function() {
    $('.dropzone').on('dragover', function() {
        $(this).addClass('dragover');
        return false;
    });

    $('.dropzone').on('dragleave', function() {
        $(this).removeClass('dragover');
        return false;
    });

    $('.dropzone').on('drop', function(e) {
        e.preventDefault();

        $(this).removeClass('dragover');

        var file = e.originalEvent.dataTransfer.files[0];
        var reader = new FileReader();

        reader.onload = function(event) {
        $('.preview-container').css('background-image', 'url(' + event.target.result + ')');
        }

        reader.readAsDataURL(file);
    });

    $('#preview-button').click(function() {
        var imageUrl = $('#image-link').val();
        $('.preview-container').css('background-image', 'url(' + imageUrl + ')');
    });
  }
));