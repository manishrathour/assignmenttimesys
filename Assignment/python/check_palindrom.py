def check_palindrom(my_str):
    updated_str = "".join([x for x in my_str if x.isalpha()]).lower()
    if updated_str == updated_str[::-1]:
        return "YES"
    return "NO"

dummy_str = "MADAM"
print(check_palindrom(dummy_str))