from random_password import generate_password

def test_generate_password():
    assert len(generate_password(8, 2, 2, 2)) == 8
    assert len(generate_password(12, 3, 3, 3)) == 12
    assert len(generate_password(16, 4, 4, 4)) == 16
