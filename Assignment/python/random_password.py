import random
import string

def generate_password(length, uppercase, digits, symbols):
    uppercase_letters = string.ascii_uppercase
    lowercase_letters = string.ascii_lowercase
    numbers = string.digits
    special_symbols = string.punctuation

    if uppercase + digits + symbols > length:
        print("Error: the sum of the required number of uppercase letters, digits, and special symbols cannot exceed the password length.")
        exit()

    password = ""

    for i in range(uppercase):
        password += random.choice(uppercase_letters)
    for i in range(digits):
        password += random.choice(numbers)
    for i in range(symbols):
        password += random.choice(special_symbols)
    for i in range(length - uppercase - digits - symbols):
        password += random.choice(uppercase_letters + lowercase_letters + numbers + special_symbols)

    password = ''.join(random.sample(password, len(password)))
    return password

length, uppercase, digits, symbols = 8,2,2,2
print(generate_password(length, uppercase, digits, symbols))
