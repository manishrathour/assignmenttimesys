from check_palindrom import check_palindrom

def test_check_palindrom():
    assert check_palindrom("A man, a plan, a canal: Panama") == "YES"
    assert check_palindrom("race a car") == "NO"
    assert check_palindrom("Was it a car or a cat I saw?") == "YES"
    assert check_palindrom("No 'x' in Nixon") == "YES"
